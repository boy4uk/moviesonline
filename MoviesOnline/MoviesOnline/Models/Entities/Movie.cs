﻿using System;
using System.Collections.Generic;

namespace MoviesOnline.Models.Entities
{
    public class Movie
    {
        public Movie(string title, List<string> tags)
        {
            Id = Guid.NewGuid();
            Title = title;
            Tags = tags;
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public List<string> Tags { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesOnline.Models.Entities
{
    public enum Role
    {
        Admin = 1,
        Moderator = 2, 
        User = 3
    }
}
     

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesOnline.Models.Entities;

namespace MoviesOnline.Infrastructure.Database.ReposInterfaces
{
    public interface IMovieRepository
    {
        bool Add(string title, List<string> tags);
        bool Delete(Movie movie);
        bool ContainMovie(string title);
        Movie Get(string title);
        Movie Get(Guid postId);
        IEnumerable<Movie> Get();
    }
}

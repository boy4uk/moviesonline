﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesOnline.Models.Entities;

namespace MoviesOnline.Infrastructure.Database.ReposInterfaces
{
    public interface IUserRepository
    {
        bool Add(string login, string password, Role role);
        bool Delete(User user);
        User Get(string login);
        User Get(Guid id);
        bool CheckPassword(string login, string password);
        bool ContainUser(string login);
    }
}

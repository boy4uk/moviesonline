﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MoviesOnline.Models.Entities;

namespace MoviesOnline.Data
{
    public class GeneralContext : DbContext
    {
        public GeneralContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                "Host=ec2-54-247-70-127.eu-west-1.compute.amazonaws.com;" +
                "Database=dd6e066vd96ca1;" +
                "Username=ccjirxqxpxyofh;" +
                "Port=5432;" +
                "Password=62050fda797689e299e87ec74526f903db3cb562ad1048f9a5bbb7fb7a278d26;" +
                "SslMode=Require;Trust Server Certificate=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Login);

            modelBuilder.Entity<Movie>()
                .HasIndex(m => m.Title);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Movie> Movies { get; set; }
    }
}

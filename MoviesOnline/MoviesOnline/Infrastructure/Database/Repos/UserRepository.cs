﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesOnline.Data;
using MoviesOnline.Infrastructure.Database.ReposInterfaces;
using MoviesOnline.Models.Entities;

namespace MoviesOnline.Infrastructure.Database.Repos
{
    public class UserRepository : IUserRepository
    {
        private GeneralContext _context;

        public UserRepository(GeneralContext context)
        {
            _context = context;
        }

        public UserRepository()
        {
            _context = new GeneralContext();
        }

        public bool Add(string login, string password, Role role)
        {
            try
            {
                if (_context.Users.Any(u => u.Login == login))
                    throw new ArgumentNullException("There is no user in db");
                _context.Users.Add(new User(login, password, role));
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public bool CheckPassword(string login, string password)
        {
            throw new NotImplementedException();
        }

        public bool ContainUser(string login) => _context.Users.Any(u => u.Login.Equals(login));

        public bool Delete(User user)
        {
            try
            {
                var userToDelete = _context.Users.FirstOrDefault(u => u.Id.Equals(user.Id)) ??
                                   throw new ArgumentNullException(nameof(user), "There is no user");
                _context.Users.Remove(user);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public User Get(string login) => _context.Users.FirstOrDefault(u => u.Login.Equals(login));

        public User Get(Guid id) => _context.Users.FirstOrDefault(u => u.Id.Equals(id));
    }
}

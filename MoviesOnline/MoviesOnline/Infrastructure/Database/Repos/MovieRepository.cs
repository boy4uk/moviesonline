﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesOnline.Infrastructure.Database.ReposInterfaces;
using MoviesOnline.Models.Entities;

namespace MoviesOnline.Infrastructure.Database.Repos
{
    public class MovieRepository : IMovieRepository
    {
        public bool Add(string title, List<string> tags)
        {
            throw new NotImplementedException();
        }

        public bool ContainMovie(string title)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Movie movie)
        {
            throw new NotImplementedException();
        }

        public Movie Get(string title)
        {
            throw new NotImplementedException();
        }

        public Movie Get(Guid postId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Movie> Get()
        {
            throw new NotImplementedException();
        }
    }
}
